/*!
 * @file
 * @brief This file contains implementation of gpu
 *
 * @author Tomáš Milet, imilet@fit.vutbr.cz
 */

#include <cstring>
#include <cstdio>
#include <student/gpu.hpp>
#include <array>


/// \addtogroup gpu_init
/// @{

/**
 * @brief Constructor of GPU
 */
GPU::GPU(){
  /// \todo Zde můžete alokovat/inicializovat potřebné proměnné grafické karty
  initBuffers();
}

/**
 * @brief Destructor of GPU
 */
GPU::~GPU(){
  /// \todo Zde můžete dealokovat/deinicializovat grafickou kartu
  freeBuffers();
}

/**
 * @brief Creator of buffersList create buffers list of size 1 with index 0 as free
 */
void GPU::initBuffers() {
  this->buffersList.buffers = (buffer_t *)malloc(sizeof(buffer_t));
  // todo: error catch ?
  this->buffersList.buffers[0].data = NULL;
  this->buffersList.buffers[0].type = NOT_BUFF;
  this->buffersList.free.push(0);
  this->buffersList.size = 1;
}

/**
 * @brief Descructor of buffersList free buffers list and buffers in it
 */
void GPU::freeBuffers() {
  for (unsigned i = 0; i < this->buffersList.size; i++) {
    if (this->buffersList.buffers[i].data != NULL)
      free(this->buffersList.buffers[i].data);
  }

  free(this->buffersList.buffers);
}

/**
 * @brief generate free ID in buffer list
 * @return BufferID index of free space in list
 */
BufferID GPU::getFreeBufIndex(){
  /* is stack with free indexis empty ? */
  if (this->buffersList.free.empty()) { // no free index add new
    /* increment size of buffer */
    this->buffersList.size++;
    /* realoc size */
    this->buffersList.buffers = (buffer_t *)realloc(this->buffersList.buffers, this->buffersList.size * sizeof(buffer_t));
    // todo: error catch ?
    /* return new index */
    return this->buffersList.size - 1;
  } else {
    /* get index from free stack */
    BufferID index = this->buffersList.free.top();
    this->buffersList.free.pop();
    return index;
  }
}

/// @}

/** \addtogroup buffer_tasks 01. Implementace obslužných funkcí pro buffery
 * @{
 */

/**
 * @brief This function allocates buffer on GPU.
 *
 * @param size size in bytes of new buffer on GPU.
 *
 * @return unique identificator of the buffer
 */
BufferID GPU::createBuffer(uint64_t size) { 
  return createBuffer(size, GENERIC_BUFF);
}

/**
 * @brief This function allocates buffer on GPU.
 *
 * @param size size in bytes of new buffer on GPU.
 * @param type type of buffer
 *
 * @return unique identificator of the buffer
 */
BufferID GPU::createBuffer(uint64_t size, uint8_t type) { 
  auto p = malloc(size);

  if ( p == NULL ) {
    return emptyID;
  }

  auto bufindex = getFreeBufIndex();

  if ( bufindex == emptyID ) {
    return emptyID;
  }

  this->buffersList.buffers[bufindex].data = p;
  this->buffersList.buffers[bufindex].type = type;
  
  return bufindex;
}

/**
 * @brief This function check buffer type
 *
 * @param buffer BufferID of buffer
 * @param type unit8_t expected buffer type
 *
 * @return bool is buffer this type
 */
bool GPU::isBufferType(BufferID buffer, uint8_t type) {
  if ((buffer != emptyID) &&
      (this->buffersList.size > buffer) &&
      (0 <= buffer) &&
      (this->buffersList.buffers[buffer].type == type)) {
    return true;
  }

  return false; 
}

/**
 * @brief This function frees allocated buffer on GPU.
 *
 * @param buffer buffer identificator
 */
void GPU::deleteBuffer(BufferID buffer) {
  free(this->buffersList.buffers[buffer].data);
  // todo: catch error?
  this->buffersList.buffers[buffer].data = NULL;
  this->buffersList.buffers[buffer].type = NOT_BUFF;
  // add to stack as free
  this->buffersList.free.push(buffer);
}

/**
 * @brief This function uploads data to selected buffer on the GPU
 *
 * @param buffer buffer identificator
 * @param offset specifies the offset into the buffer's data
 * @param size specifies the size of buffer that will be uploaded
 * @param data specifies a pointer to new data
 */
void GPU::setBufferData(BufferID buffer, uint64_t offset, uint64_t size, void const* data) {
  std::memcpy((uint8_t *)this->buffersList.buffers[buffer].data + offset, data, size);
}

/**
 * @brief This function downloads data from GPU.
 *
 * @param buffer specfies buffer
 * @param offset specifies the offset into the buffer from which data will be returned, measured in bytes. 
 * @param size specifies data size that will be copied
 * @param data specifies a pointer to the location where buffer data is returned. 
 */
void GPU::getBufferData(BufferID buffer,
                        uint64_t offset,
                        uint64_t size,
                        void*    data)
{
  std::memcpy(data, (uint8_t *)this->buffersList.buffers[buffer].data + offset, size);
}

/**
 * @brief This function tests if buffer exists
 *
 * @param buffer selected buffer id
 *
 * @return true if buffer points to existing buffer on the GPU.
 */
bool GPU::isBuffer(BufferID buffer) { 
  return isBufferType(buffer, GENERIC_BUFF);
}

/// @}

/**
 * \addtogroup vertexpuller_tasks 02. Implementace obslužných funkcí pro vertex puller
 * @{
 */

/**
 * @brief This function creates new vertex puller settings on the GPU,
 *
 * @return unique vertex puller identificator
 */
ObjectID GPU::createVertexPuller     (){
  ObjectID vao = createBuffer(
    sizeof(VertexPullerSettings_t),
    VERTEX_PULLER_SET_BUFF
  );

  //init defaults
  VertexPullerSettings_t defaults;

  defaults.indexing.enabled = false;
  
  for (int i = 0; i < maxAttributes; i++) {
    defaults.heads[i].enabled = false;
  }

  setBufferData(vao, 0, sizeof(VertexPullerSettings_t), &defaults);

  return vao;
}

/**
 * @brief This function deletes vertex puller settings
 *
 * @param vao vertex puller identificator
 */
void     GPU::deleteVertexPuller     (VertexPullerID vao){
  deleteBuffer(vao);
}

/**
 * @brief This function sets one vertex puller reading head.
 *
 * @param vao identificator of vertex puller
 * @param head id of vertex puller head
 * @param type type of attribute
 * @param stride stride in bytes
 * @param offset offset in bytes
 * @param buffer id of buffer
 */
void     GPU::setVertexPullerHead    (VertexPullerID vao,uint32_t head,AttributeType type,uint64_t stride,uint64_t offset,BufferID buffer){
  auto selhead = & ((VertexPullerSettings_t *)this->buffersList.buffers[vao].data)->heads[head];
  selhead->type   = type;
  selhead->stride = stride;
  selhead->offset = offset;
  selhead->buffer = buffer;
}

/**
 * @brief This function sets vertex puller indexing.
 *
 * @param vao vertex puller id
 * @param type type of index
 * @param buffer buffer with indices
 */
void     GPU::setVertexPullerIndexing(VertexPullerID vao,IndexType type,BufferID buffer){
  auto indexing = & ((VertexPullerSettings_t *)this->buffersList.buffers[vao].data)->indexing;
  indexing->type    = type;
  indexing->buffer  = buffer;
  indexing->enabled = true;
}

/**
 * @brief This function enables vertex puller's head.
 *
 * @param vao vertex puller 
 * @param head head id
 */
void     GPU::enableVertexPullerHead (VertexPullerID vao,uint32_t head){
  auto selhead = & ((VertexPullerSettings_t *)this->buffersList.buffers[vao].data)->heads[head];
  selhead->enabled = true;
}

/**
 * @brief This function disables vertex puller's head
 *
 * @param vao vertex puller id
 * @param head head id
 */
void     GPU::disableVertexPullerHead(VertexPullerID vao,uint32_t head){
  auto selhead = & ((VertexPullerSettings_t *)this->buffersList.buffers[vao].data)->heads[head];
  selhead->enabled = false;
}

/**
 * @brief This function selects active vertex puller.
 *
 * @param vao id of vertex puller
 */
void     GPU::bindVertexPuller       (VertexPullerID vao){
  this->activeVao = vao;
}

/**
 * @brief This function deactivates vertex puller.
 */
void     GPU::unbindVertexPuller     (){
  this->activeVao = emptyID;
}

/**
 * @brief This function tests if vertex puller exists.
 *
 * @param vao vertex puller
 *
 * @return true, if vertex puller "vao" exists
 */
bool     GPU::isVertexPuller         (VertexPullerID vao){
  return isBufferType(vao, VERTEX_PULLER_SET_BUFF);
}

/// @}

/** \addtogroup program_tasks 03. Implementace obslužných funkcí pro shader programy
 * @{
 */

/**
 * @brief This function creates new shader program.
 *
 * @return shader program id
 */
ProgramID        GPU::createProgram         (){
  ProgramID prg = createBuffer(
    sizeof(programSettings_t),
    PROGRAM_SET_BUFF 
  );

  //init defaults
  programSettings_t defaults;

  for (int i = 0; i < maxUniforms; i++) {
    defaults.attrType[i] = AttributeType::EMPTY;
  }

  setBufferData(prg, 0, sizeof(programSettings_t), &defaults);

  return prg;
}

/**
 * @brief This function deletes shader program
 *
 * @param prg shader program id
 */
void             GPU::deleteProgram         (ProgramID prg){
  deleteBuffer(prg);
}

/**
 * @brief This function attaches vertex and frament shader to shader program.
 *
 * @param prg shader program
 * @param vs vertex shader 
 * @param fs fragment shader
 */
void             GPU::attachShaders         (ProgramID prg,VertexShader vs,FragmentShader fs){
  auto program = (programSettings_t *)this->buffersList.buffers[prg].data;
  program->vertexShader = vs;
  program->fragmentShader = fs;
}

/**
 * @brief This function selects which vertex attributes should be interpolated during rasterization into fragment attributes.
 *
 * @param prg shader program
 * @param attrib id of attribute
 * @param type type of attribute
 */
void             GPU::setVS2FSType          (ProgramID prg,uint32_t attrib,AttributeType type){
  /// \todo tato funkce by měla zvolit typ vertex atributu, který je posílán z vertex shaderu do fragment shaderu.<br>
  /// V průběhu rasterizace vznikají fragment.<br>
  /// Fragment obsahují fragment atributy.<br>
  /// Tyto atributy obsahují interpolované hodnoty vertex atributů.<br>
  /// Tato funkce vybere jakého typu jsou tyto interpolované atributy.<br>
  /// Bez jakéhokoliv nastavení jsou atributy prázdne AttributeType::EMPTY<br>
  auto program = (programSettings_t *)this->buffersList.buffers[prg].data;
  program->attrType[attrib] = type;
}

/**
 * @brief This function actives selected shader program
 *
 * @param prg shader program id
 */
void             GPU::useProgram            (ProgramID prg){
  this->activePrg = prg;
}

/**
 * @brief This function tests if selected shader program exists.
 *
 * @param prg shader program
 *
 * @return true, if shader program "prg" exists.
 */
bool             GPU::isProgram             (ProgramID prg){
  return isBufferType(prg, PROGRAM_SET_BUFF);
}

/**
 * @brief This function sets uniform value (1 float).
 *
 * @param prg shader program
 * @param uniformId id of uniform value (number of uniform values is stored in maxUniforms variable)
 * @param d value of uniform variable
 */
void             GPU::programUniform1f      (ProgramID prg,uint32_t uniformId,float     const&d){
  /// \todo tato funkce by měla nastavit uniformní proměnnou shader programu.<br>
  /// Parametr "prg" vybírá shader program.<br>
  /// Parametr "uniformId" vybírá uniformní proměnnou. Maximální počet uniformních proměnných je uložen v programné \link maxUniforms \endlink.<br>
  /// Parametr "d" obsahuje data (1 float).<br>
  auto program = (programSettings_t *)this->buffersList.buffers[prg].data;
  program->vars.uniform[uniformId].v1 = d;
}

/**
 * @brief This function sets uniform value (2 float).
 *
 * @param prg shader program
 * @param uniformId id of uniform value (number of uniform values is stored in maxUniforms variable)
 * @param d value of uniform variable
 */
void             GPU::programUniform2f      (ProgramID prg,uint32_t uniformId,glm::vec2 const&d){
  auto program = (programSettings_t *)this->buffersList.buffers[prg].data;
  program->vars.uniform[uniformId].v2 = d;
}

/**
 * @brief This function sets uniform value (3 float).
 *
 * @param prg shader program
 * @param uniformId id of uniform value (number of uniform values is stored in maxUniforms variable)
 * @param d value of uniform variable
 */
void             GPU::programUniform3f      (ProgramID prg,uint32_t uniformId,glm::vec3 const&d){
  auto program = (programSettings_t *)this->buffersList.buffers[prg].data;
  program->vars.uniform[uniformId].v3 = d;
}

/**
 * @brief This function sets uniform value (4 float).
 *
 * @param prg shader program
 * @param uniformId id of uniform value (number of uniform values is stored in maxUniforms variable)
 * @param d value of uniform variable
 */
void             GPU::programUniform4f      (ProgramID prg,uint32_t uniformId,glm::vec4 const&d){
  auto program = (programSettings_t *)this->buffersList.buffers[prg].data;
  program->vars.uniform[uniformId].v4 = d;
}

/**
 * @brief This function sets uniform value (4 float).
 *
 * @param prg shader program
 * @param uniformId id of uniform value (number of uniform values is stored in maxUniforms variable)
 * @param d value of uniform variable
 */
void             GPU::programUniformMatrix4f(ProgramID prg,uint32_t uniformId,glm::mat4 const&d){
  auto program = (programSettings_t *)this->buffersList.buffers[prg].data;
  program->vars.uniform[uniformId].m4 = d;
}

/// @}





/** \addtogroup framebuffer_tasks 04. Implementace obslužných funkcí pro framebuffer
 * @{
 */

/**
 * @brief This function creates framebuffer on GPU.
 *
 * @param width width of framebuffer
 * @param height height of framebuffer
 */
void GPU::createFramebuffer      (uint32_t width,uint32_t height){
  /// \todo Tato funkce by měla alokovat framebuffer od daném rozlišení.<br>
  /// Framebuffer se skládá z barevného a hloukového bufferu.<br>
  /// Buffery obsahují width x height pixelů.<br>
  /// Barevný pixel je složen z 4 x uint8_t hodnot - to reprezentuje RGBA barvu.<br>
  /// Hloubkový pixel obsahuje 1 x float - to reprezentuje hloubku.<br>
  /// Nultý pixel framebufferu je vlevo dole.<br>
  auto color = (uint8_t*) malloc(width * height * sizeof(uint8_t) * 4);

  if ( color == NULL ) {
    return;
  }

  auto depth = (float*) malloc(width * height * sizeof(float));

  if ( depth == NULL ) {
    free(color);
    return;
  }

  this->frameBuff.color  = color;
  this->frameBuff.depth  = depth;
  this->frameBuff.width  = width;
  this->frameBuff.height = height;

  clear(0,0,0,0);
}

/**
 * @brief This function deletes framebuffer.
 */
void GPU::deleteFramebuffer      (){
  free(this->frameBuff.color);
  free(this->frameBuff.depth);
  this->frameBuff.width  = 0;
  this->frameBuff.height = 0;
}

/**
 * @brief This function resizes framebuffer.
 *
 * @param width new width of framebuffer
 * @param height new heght of framebuffer
 */
void     GPU::resizeFramebuffer(uint32_t width,uint32_t height){
  auto color = (uint8_t*) realloc(this->frameBuff.color, width * height * sizeof(uint8_t) * 4);

  if ( color == NULL ) {
    return;
  }

  auto depth = (float*) realloc(this->frameBuff.depth, width * height * sizeof(float));

  if ( depth == NULL ) {
    return;
  }

  this->frameBuff.color  = color;
  this->frameBuff.depth  = depth;
  this->frameBuff.width  = width;
  this->frameBuff.height = height;

  clear(0,0,0,0);
}

/**
 * @brief This function returns pointer to color buffer.
 *
 * @return pointer to color buffer
 */
uint8_t* GPU::getFramebufferColor  (){
  return this->frameBuff.color;
}

/**
 * @brief This function returns pointer to depth buffer.
 *
 * @return pointer to dept buffer.
 */
float* GPU::getFramebufferDepth    (){
  return this->frameBuff.depth;
}

/**
 * @brief This function returns width of framebuffer
 *
 * @return width of framebuffer
 */
uint32_t GPU::getFramebufferWidth (){
  return this->frameBuff.width;
}

/**
 * @brief This function returns height of framebuffer.
 *
 * @return height of framebuffer
 */
uint32_t GPU::getFramebufferHeight(){
  return this->frameBuff.height;
}

float GPU::getFramebufferDepthXY (int x, int y){
  return this->frameBuff.depth[y * this->frameBuff.width + x];
}

/**
 * @brief This function set pixel on frameubffer and deth buffer
 *
 * @param x unsined coordinate x
 * @param y unsined coordinate y
 * @param r uint8_t red of pixel 0 - 255
 * @param g uint8_t green of pixel 0 - 255
 * @param b uint8_t blue of pixel 0 - 255
 * @param a uint8_t alpha of pixel 0 - 255
 * @param depth float depth of pixel
 *
 * @return void
 */
void GPU::frameBuffsetPixel(unsigned x, unsigned y, uint8_t r, uint8_t g, uint8_t b, uint8_t a, float depth) {
  if ((x < this->frameBuff.width) && (y < this->frameBuff.height)) {
    auto colorPixel = & this->frameBuff.color[4 * (y * this->frameBuff.width + x)];
    
    colorPixel[0] = r;
    colorPixel[1] = g;
    colorPixel[2] = b;
    colorPixel[3] = a;

    this->frameBuff.depth[y * this->frameBuff.width + x] = depth;
  }
}

/// @}

/** \addtogroup draw_tasks 05. Implementace vykreslovacích funkcí
 * Bližší informace jsou uvedeny na hlavní stránce dokumentace.
 * @{
 */

/**
 * @brief This functino clears framebuffer.
 *
 * @param r red channel
 * @param g green channel
 * @param b blue channel
 * @param a alpha channel
 */
void            GPU::clear                 (float r,float g,float b,float a){
  /// \todo Tato funkce by měla vyčistit framebuffer.<br>
  /// Barevný buffer vyčistí na barvu podle parametrů r g b a (0 - nulová intenzita, 1 a větší - maximální intenzita).<br>
  /// (0,0,0) - černá barva, (1,1,1) - bílá barva.<br>
  /// Hloubkový buffer nastaví na takovou hodnotu, která umožní rasterizaci trojúhelníka, který leží v rámci pohledového tělesa.<br>
  /// Hloubka by měla být tedy větší než maximální hloubka v NDC (normalized device coordinates).<br>
  for (unsigned x = 0; x < this->frameBuff.width; x++) {
    for (unsigned y = 0; y < this->frameBuff.height; y++) {
      frameBuffsetPixel(
        x,
        y,
        (uint8_t)(r * 255),
        (uint8_t)(g * 255),
        (uint8_t)(b * 255),
        (uint8_t)(a * 255),
        2 //depth
      );
    }
  }
}

/**
 * @brief simulate vertex puller (GET data from buffers by heads) - get vertex
 *
 * @param jump unsined address to read (4 - read 4th vertex)
 *
 * @return InVertex loaded vertex
 */
InVertex GPU::vertexPuller(unsigned jump) {

  auto vao = (VertexPullerSettings_t *)this->buffersList.buffers[this->activeVao].data;

  InVertex in;

  unsigned index = jump;
  if (vao->indexing.enabled == true) {
    getBufferData(
      vao->indexing.buffer,
      static_cast<int>(vao->indexing.type) * index,
      static_cast<int>(vao->indexing.type),
      &index
    );
  }

  in.gl_VertexID = index;

  for (unsigned actHead = 0; actHead < maxAttributes; actHead++) {
    if (vao->heads[actHead].enabled == true) {
      auto type = vao->heads[actHead].type;

      switch (type) {
        case AttributeType::FLOAT :
          getBufferData(
            vao->heads[actHead].buffer,
            vao->heads[actHead].offset + vao->heads[actHead].stride * index,
            sizeof(float),
            &in.attributes[actHead].v1
          );

          break;
        case AttributeType::VEC2 :
          getBufferData(
            vao->heads[actHead].buffer,
            vao->heads[actHead].offset + vao->heads[actHead].stride * index,
            sizeof(glm::vec2),
            &in.attributes[actHead].v2
          );

          break;
        case AttributeType::VEC3 :
          getBufferData(
            vao->heads[actHead].buffer,
            vao->heads[actHead].offset + vao->heads[actHead].stride * index,
            sizeof(glm::vec3),
            &in.attributes[actHead].v3
          );
          
          break;
        case AttributeType::VEC4 :
          getBufferData(
            vao->heads[actHead].buffer,
            vao->heads[actHead].offset + vao->heads[actHead].stride * index,
            sizeof(glm::vec4),
            &in.attributes[actHead].v4
          );   
      }
    }
  }

  return in;
}

/**
 * @brief draw horizontal line (for scanline rasterization) for every pixel call fragmentProcessor()
 *
 * @param x1 float 1st point of line in X cooridinate
 * @param x2 float 2nd point of line in X cooridinate
 * @param y  unsigned Y coordinate of line
 *
 * @return void
 */
void GPU::drawLine (float x1, float x2, unsigned y) {
	if (x1 > x2) {
		std::swap(x1, x2);
	}

	for (int x = round(x1); x + 0.5 <= x2; x++) {
    fragmentProcessor(x, y);
	}
}

/**
 * @brief drow triangle with base on bottom using drawLine()
 *        +
 *       / \
 *      /   \
 *     +-----+
 *
 * @param v1 glm::vec3 1st vertex
 * @param v2 glm::vec3 2nd vertex
 * @param v3 glm::vec3 3rd vertex
 *
 * @return void
 */
void GPU::scanLine_down(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3) {
  float invslope1 = (v2[0] - v1[0]) / (v2[1] - v1[1]);
  float invslope2 = (v3[0] - v1[0]) / (v3[1] - v1[1]);

  float curx1 = v2[0];
  float curx2 = v3[0];

  for (int scanlineY = floor(v2[1]); scanlineY <= ceil(v1[1]); scanlineY++) {
    drawLine(curx1, curx2, scanlineY);
    curx1 += invslope1;
    curx2 += invslope2;
  }
}

/**
 * @brief drow triangle with base on top using drawLine()
 *     +-----+
 *      \   /
 *       \ /
 *        +
 *
 * @param v1 glm::vec3 1st vertex
 * @param v2 glm::vec3 2nd vertex
 * @param v3 glm::vec3 3rd vertex
 *
 * @return void
 */
void GPU::scanLine_up(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3) {
  float invslope1 = (v3[0] - v1[0]) / (v3[1] - v1[1]);
  float invslope2 = (v3[0] - v2[0]) / (v3[1] - v2[1]);

  float curx1 = v1[0];
  float curx2 = v2[0];

  for (int scanlineY = floor(v1[1]); scanlineY >= ceil(v3[1]); scanlineY--) {
    drawLine(curx1, curx2, scanlineY);
    curx1 -= invslope1;
    curx2 -= invslope2;
  }
}

/**
 * @brief drow triangle what do not have base using drawLine()
 *        It is more slow thet scanLine_up() or scanLine_down()
 *
 *         +
 *        /|
 *       / |
 *      /  |
 *     +   |
 *      \  |
 *       \ |
 *        \|
 *         +
 *
 * @param v1 glm::vec3 1st vertex
 * @param v2 glm::vec3 2nd vertex
 * @param v3 glm::vec3 3rd vertex
 *
 * @return void
 */
void GPU::scanLine_generic(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3) {
  float invslope1 = (v3[0] - v1[0]) / (v3[1] - v1[1]);

  float invslope2_1 = (v2[0] - v1[0]) / (v2[1] - v1[1]);
  float invslope2_2 = (v3[0] - v2[0]) / (v3[1] - v2[1]);

  float curx1 = v1[0];
  float curx2 = v1[0];

  for (int scanlineY = floor(v1[1]); scanlineY >= ceil(v2[1]); scanlineY--) {
    drawLine(curx1, curx2, scanlineY);
    curx1 -= invslope1;
    curx2 -= invslope2_1;
  }

  curx2 = v2[0];

  for (int scanlineY = floor(v2[1]); scanlineY >= ceil(v3[1]); scanlineY--) {
    drawLine(curx1, curx2, scanlineY);
    curx1 -= invslope1;
    curx2 -= invslope2_2;
  }
}

/**
 * @brief drow triangle using scanLine_up() or scanLine_down() or scanLine_generic() it select auto
 *
 * @param v1 glm::vec3 1st vertex
 * @param v2 glm::vec3 2nd vertex
 * @param v3 glm::vec3 3rd vertex
 *
 * @return void
 */
void GPU::rasterize (OutVertex v1, OutVertex v2, OutVertex v3) {

  //sort it
  if (v1.gl_Position[1] < v2.gl_Position[1])
    std::swap(v1, v2);

  if (v2.gl_Position[1] < v3.gl_Position[1])
    std::swap(v2, v3);
  
  if (v1.gl_Position[1] < v2.gl_Position[1])
    std::swap(v1, v2);


  if (v2.gl_Position[1] == v3.gl_Position[1]) {
    
    scanLine_down(v1.gl_Position, v2.gl_Position, v3.gl_Position);

  } else if (v1.gl_Position[1] == v2.gl_Position[1]) {
    
    scanLine_up(v1.gl_Position, v2.gl_Position, v3.gl_Position);

  } else {

    scanLine_generic(v1.gl_Position, v2.gl_Position, v3.gl_Position);
    
  }
}

/**
 * @brief fragment processor for rasterized pixel it do clipping, interpolate attr form VS to FS, perspective correction, invoke the fragment shader, and set pixel to framebuffer
 *
 * @param x int coordinate of pixel
 * @param y int coordinate of pixel
 *
 * @return void
 */
void            GPU::fragmentProcessor(int x, int y) {
  auto program = (programSettings_t *)this->buffersList.buffers[this->activePrg].data;

  // clip to window size
  if (!((x >= 0) && (x < getFramebufferWidth()) && (y >= 0) && (y < getFramebufferHeight())))
    return;

  InFragment fragment; 

  fragment.gl_FragCoord[0] = x + 0.5;
  fragment.gl_FragCoord[1] = y + 0.5;

  // fragment barycentric coordinates in window coordinates
  const glm::vec3 barycentric = fragment.gl_FragCoord[0] * this->barycentric_x + fragment.gl_FragCoord[1] *  this->barycentric_y +  this->barycentric_0;

  // interpolate inverse depth linearly
  fragment.gl_FragCoord[3] = glm::dot(barycentric, glm::vec3(this->vertexis[0].gl_Position[3], this->vertexis[1].gl_Position[3], this->vertexis[2].gl_Position[3]));
  //for clipping
  fragment.gl_FragCoord[2] = glm::dot(barycentric, glm::vec3(this->vertexis[0].gl_Position[2], this->vertexis[1].gl_Position[2], this->vertexis[2].gl_Position[2]));

  // clip fragments to the near/far planes
  if(fragment.gl_FragCoord[2] < -1 || fragment.gl_FragCoord[2] > 1)
    return;

  // convert to perspective correct (clip-space) barycentric
  const glm::vec3 perspective = 1 / fragment.gl_FragCoord[3] * barycentric * glm::vec3(this->vertexis[0].gl_Position[3], this->vertexis[1].gl_Position[3], this->vertexis[2].gl_Position[3]);

  //perspective correct Z
  fragment.gl_FragCoord[2] = glm::dot(perspective, glm::vec3(this->vertexis[0].gl_Position[2], this->vertexis[1].gl_Position[2], this->vertexis[2].gl_Position[2]));

  // deph test
  if(fragment.gl_FragCoord[2] >= getFramebufferDepthXY(fragment.gl_FragCoord[0], fragment.gl_FragCoord[1]))
    return;
    
  // interpolate the attributes using the perspective correct barycentric
  for(int i = 0; i < maxAttributes; i++) {
    switch (program->attrType[i]) {
      case AttributeType::FLOAT :
        fragment.attributes[i].v1 = glm::dot(perspective, glm::vec3(
          vertexis[0].attributes[i].v1,
          vertexis[1].attributes[i].v1,
          vertexis[2].attributes[i].v1
        ));
        break;
      
      case AttributeType::VEC2 :
        for (int j = 0; j < 2; j++)
          fragment.attributes[i].v2[j] = glm::dot(perspective, glm::vec3(
            vertexis[0].attributes[i].v2[j],
            vertexis[1].attributes[i].v2[j],
            vertexis[2].attributes[i].v2[j]
          ));
          break;

      case AttributeType::VEC3 :
        for (int j = 0; j < 3; j++)
          fragment.attributes[i].v3[j] = glm::dot(perspective, glm::vec3(
            vertexis[0].attributes[i].v3[j],
            vertexis[1].attributes[i].v3[j],
            vertexis[2].attributes[i].v3[j]
          ));
          break;
      
      case AttributeType::VEC4 :
        for (int j = 0; j < 4; j++)
          fragment.attributes[i].v4[j] = glm::dot(perspective, glm::vec3(
            vertexis[0].attributes[i].v4[j],
            vertexis[1].attributes[i].v4[j],
            vertexis[2].attributes[i].v4[j]
          ));
      }
    }

  // invoke the fragment shader and store the result
  OutFragment color;
  program->fragmentShader(
    color,
    fragment,
    program->vars
  );

  frameBuffsetPixel(
    fragment.gl_FragCoord[0],
    fragment.gl_FragCoord[1],
    round(color.gl_FragColor[0] * 255),
    round(color.gl_FragColor[1] * 255),
    round(color.gl_FragColor[2] * 255),
    round(color.gl_FragColor[3] * 255),
    fragment.gl_FragCoord[2]
  );
}

/**
 * @brief 1st part of pipeline it do primitive assembly, perspective division, convert to window coordinate, fastclipping, precompute the affine transform from fragment coordinates to barycentric coordinates and call rasterize()
 *
 * @param nofVertices uint32_t number of vertexis to process
 *
 * @return void
 */
void            GPU::drawTriangles         (uint32_t  nofVertices){
  /// \todo Tato funkce vykreslí trojúhelníky podle daného nastavení.<br>
  /// Vrcholy se budou vybírat podle nastavení z aktivního vertex pulleru (pomocí bindVertexPuller).<br>
  /// Vertex shader a fragment shader se zvolí podle aktivního shader programu (pomocí useProgram).<br>
  /// Parametr "nofVertices" obsahuje počet vrcholů, který by se měl vykreslit (3 pro jeden trojúhelník).<br>

  auto program = (programSettings_t *)this->buffersList.buffers[this->activePrg].data;

  for (int v = 0; v < nofVertices / 3; v++) {
    
    OutVertex vertexis[3];

    //primitive assembly
    for(int i = 0; i < 3; i++) {
      //vertex puller
      auto in = vertexPuller(v * 3 + i);

      //vertex shader
      program->vertexShader(
        vertexis[i],
        in,
        program->vars
      );
      /* primitivy assebly end */

      // convert to device coordinates by perspective division
      vertexis[i].gl_Position[3] = 1 / vertexis[i].gl_Position[3];
      vertexis[i].gl_Position[0] *=    vertexis[i].gl_Position[3];
      vertexis[i].gl_Position[1] *=    vertexis[i].gl_Position[3];
      vertexis[i].gl_Position[2] *=    vertexis[i].gl_Position[3];

      // convert to window coordinates
      vertexis[i].gl_Position[0] = (vertexis[i].gl_Position[0] + 1) * getFramebufferWidth()  * 0.5;
      vertexis[i].gl_Position[1] = (vertexis[i].gl_Position[1] + 1) * getFramebufferHeight() * 0.5;
    }

    int xmin = std::min(vertexis[0].gl_Position[0], std::min(vertexis[1].gl_Position[0], vertexis[2].gl_Position[0]));
    int ymin = std::min(vertexis[0].gl_Position[1], std::min(vertexis[1].gl_Position[1], vertexis[2].gl_Position[1]));
    int xmax = std::max(vertexis[0].gl_Position[0], std::max(vertexis[1].gl_Position[0], vertexis[2].gl_Position[0]));
    int ymax = std::max(vertexis[0].gl_Position[1], std::max(vertexis[1].gl_Position[1], vertexis[2].gl_Position[1]));

    //fast clipping allow only when is near or in frame
    //same can be on Z too
    if(ymin < -MAX_ALLOW_RASTERIZE || xmin < -MAX_ALLOW_RASTERIZE || ymax > getFramebufferHeight() + MAX_ALLOW_RASTERIZE || xmax > getFramebufferWidth() + MAX_ALLOW_RASTERIZE || 
      !std::isfinite(ymin) || !std::isfinite(ymax) || !std::isfinite(xmin) || !std::isfinite(xmax))
      continue;

    // precompute the affine transform from fragment coordinates to barycentric coordinates
    const float denom = 1 / ((vertexis[0].gl_Position[0] - vertexis[2].gl_Position[0])*(vertexis[1].gl_Position[1] - vertexis[0].gl_Position[1]) - (vertexis[0].gl_Position[0] - vertexis[1].gl_Position[0])*(vertexis[2].gl_Position[1] - vertexis[0].gl_Position[1]));
    this->barycentric_x = denom * glm::vec3(
      vertexis[1].gl_Position[1] - vertexis[2].gl_Position[1],
      vertexis[2].gl_Position[1] - vertexis[0].gl_Position[1],
      vertexis[0].gl_Position[1] - vertexis[1].gl_Position[1]
    );

    this->barycentric_y = denom * glm::vec3(
      vertexis[2].gl_Position[0] - vertexis[1].gl_Position[0],
      vertexis[0].gl_Position[0] - vertexis[2].gl_Position[0],
      vertexis[1].gl_Position[0] - vertexis[0].gl_Position[0]
    );
    
    this->barycentric_0 =  denom * glm::vec3(
      vertexis[1].gl_Position[0] * vertexis[2].gl_Position[1] - vertexis[2].gl_Position[0] * vertexis[1].gl_Position[1],
      vertexis[2].gl_Position[0] * vertexis[0].gl_Position[1] - vertexis[0].gl_Position[0] * vertexis[2].gl_Position[1],
      vertexis[0].gl_Position[0] * vertexis[1].gl_Position[1] - vertexis[1].gl_Position[0] * vertexis[0].gl_Position[1]
    );

    // shared for fragmentProcessor()
    this->vertexis = vertexis;

    rasterize(vertexis[0], vertexis[1], vertexis[2]);

  }

}

/// @}
