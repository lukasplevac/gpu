/*!
 * @file
 * @brief This file contains class that represents graphic card.
 *
 * @author Tomáš Milet, imilet@fit.vutbr.cz
 */
#pragma once

#include <student/fwd.hpp>
#include <vector>

/** define buffers types **/
#define NOT_BUFF               0
#define GENERIC_BUFF           1
#define VERTEX_PULLER_SET_BUFF 2
#define PROGRAM_SET_BUFF       3

#define MAX_ALLOW_RASTERIZE    120 //max allow cooridinates to rasterize outside viewport (+ and  -)


// own data types

typedef struct {
  void                 *data;                 /* pointer to data */
  uint8_t               type;                 /* type of buffer */
  unsigned              size;
} buffer_t;


typedef struct {
  buffer_t             *buffers;               /* array of buffer_t */
  BufferID              size;                  /* size of array */
  std::stack <BufferID> free;                  /* stack with free indexes */
} buffersList_t;

typedef struct {
  BufferID             buffer;                  /* id of indexing buffer */
  IndexType            type;                    /* type of indexing */
  bool                 enabled;
} indexingSettings_t;

typedef struct {
  AttributeType        type;                    /* type of attribute */
  uint64_t             stride;                  /* stride in bytes */
  uint64_t             offset;                  /* offset in bytes */
  BufferID 	           buffer;                  /* id of buffer */
  bool                 enabled;                 /* is head enabled */
} headSettings_t;

typedef struct {
  indexingSettings_t   indexing;                /* indexing settings */
  headSettings_t       heads[maxAttributes];    /* heards settings */
} VertexPullerSettings_t;

typedef struct {
  VertexShader 	       vertexShader;
  FragmentShader 	     fragmentShader;
  Uniforms             vars;                    /* This represents shader program uniform variables */
  AttributeType        attrType[maxUniforms];
} programSettings_t;

typedef struct {
  float               *depth    = nullptr;      /* Depth buffer */
  uint8_t             *color    = nullptr;      /* color buffer */
  unsigned             width    = 0;            /* width of buffers */
  unsigned             height   = 0;            /* height of buffers */
} frameBuffer_t;

/**
 * @brief This class represent software GPU
 */
class GPU{
  public:
    GPU();
    virtual ~GPU();

    //buffer object commands
    BufferID  createBuffer           (uint64_t size);
    void      deleteBuffer           (BufferID buffer);
    void      setBufferData          (BufferID buffer,uint64_t offset,uint64_t size,void const* data);
    void      getBufferData          (BufferID buffer,uint64_t offset,uint64_t size,void      * data);
    bool      isBuffer               (BufferID buffer);

    //vertex array object commands (vertex puller)
    ObjectID  createVertexPuller     ();
    void      deleteVertexPuller     (VertexPullerID vao);
    void      setVertexPullerHead    (VertexPullerID vao,uint32_t head,AttributeType type,uint64_t stride,uint64_t offset,BufferID buffer);
    void      setVertexPullerIndexing(VertexPullerID vao,IndexType type,BufferID buffer);
    void      enableVertexPullerHead (VertexPullerID vao,uint32_t head);
    void      disableVertexPullerHead(VertexPullerID vao,uint32_t head);
    void      bindVertexPuller       (VertexPullerID vao);
    void      unbindVertexPuller     ();
    bool      isVertexPuller         (VertexPullerID vao);

    //program object commands
    ProgramID createProgram          ();
    void      deleteProgram          (ProgramID prg);
    void      attachShaders          (ProgramID prg,VertexShader vs,FragmentShader fs);
    void      setVS2FSType           (ProgramID prg,uint32_t attrib,AttributeType type);
    void      useProgram             (ProgramID prg);
    bool      isProgram              (ProgramID prg);
    void      programUniform1f       (ProgramID prg,uint32_t uniformId,float     const&d);
    void      programUniform2f       (ProgramID prg,uint32_t uniformId,glm::vec2 const&d);
    void      programUniform3f       (ProgramID prg,uint32_t uniformId,glm::vec3 const&d);
    void      programUniform4f       (ProgramID prg,uint32_t uniformId,glm::vec4 const&d);
    void      programUniformMatrix4f (ProgramID prg,uint32_t uniformId,glm::mat4 const&d);

    //framebuffer functions
    void      createFramebuffer      (uint32_t width,uint32_t height);
    void      deleteFramebuffer      ();
    void      resizeFramebuffer      (uint32_t width,uint32_t height);
    uint8_t*  getFramebufferColor    ();
    float*    getFramebufferDepth    ();
    uint32_t  getFramebufferWidth    ();
    uint32_t  getFramebufferHeight   ();

    //execution commands
    void      clear                  (float r,float g,float b,float a);
    void      drawTriangles          (uint32_t  nofVertices);

    /// \addtogroup gpu_init 00. proměnné, inicializace / deinicializace grafické karty
    /// @{
    /// \todo zde si můžete vytvořit proměnné grafické karty (buffery, programy, ...)
    /// @}
    void             frameBuffsetPixel(unsigned x, unsigned y, uint8_t r, uint8_t g, uint8_t b, uint8_t a, float depth);
    //generalize isBuffer
    bool             isBufferType(BufferID buffer, uint8_t type);
    //own version of createbuffer
    BufferID         createBuffer(uint64_t size, uint8_t type);
  private:
    /* shared memory for pipeline */
    glm::vec3 barycentric_x;
    glm::vec3 barycentric_y;
    glm::vec3 barycentric_0;
    OutVertex *vertexis;

    /* functions */
    BufferID         getFreeBufIndex();
    void             initBuffers();
    void             freeBuffers();
    float            getFramebufferDepthXY (int x, int y);
    float            edgeFunction(glm::vec4 a, glm::vec4 b, glm::vec4 c);
    InVertex         vertexPuller(unsigned jump);


void drawLine (float x1, float x2, unsigned y);
    void             drawTriangle (unsigned x1, unsigned y1, unsigned x2, unsigned y2, unsigned x3, unsigned y3);
void scanLine_down(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3);
void scanLine_up(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3);
void fragmentProcessor(int x, int y);
void rasterize (OutVertex v1, OutVertex v2, OutVertex v3);
void            interpolateAttr(glm::vec3 perspective, InFragment fragment) ;
void scanLine_generic(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3);


    /* data */
    buffersList_t    buffersList;
    VertexPullerID   activeVao     = emptyID;
    ProgramID        activePrg     = emptyID;
    frameBuffer_t    frameBuff;

};


