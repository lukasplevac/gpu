# IZG project - software implementation of GPU

## Assignment of the project to the subject IZG. (EN - Google Transplate)

It looks for them to implement a simple graphics card (gpu). Other gpu visualize a rabbit model with a phong illuminating image and a phong shading with a procedural texture. The goals of the project are to learn how today's graphics cards work and how to control these cards using the API (which is close to OpenGL). Anyone who is eligible for the acceptance test may be able to verify the functionality of your implementation. Next I recommend exploring how this project is done. You can learn how to do CMake, how to do unit tests, how to write doxygen.

More in the file start_here.html (CZ only)

## Zadání projektu do předmětu IZG. (CZ)

Vašim úkolem je naimplementovat jednoduchou grafickou kartu (gpu). Pomocí gpu vizualizovat model králička s phongovým osvělovacím modelem a phongovým stínováním s procedurální texturou. Cílem projektu je naučit vás jak fungují dnešní grafické karty a jak tyto karty ovládat pomocí API (které je blízké k OpenGL). Každý úkol má přiřazen akceptační test, takže si můžete snadno ověřit funkčnosti vaší implementace. Dále doporučuji prozkoumání, jak je tento projekt udělán. Můžete se naučit jak udělat CMake, jak udělat unit testy, jak psát doxygen. 

Více v souboru start_here.html


## Kompilace (compilation)

´´´sh
./merlinCompilationTest.sh
´´´

## Spuštění (start/run)

´´´sh
./build/izgProject
´´´

## Ukázky z projektu (samples from project)

in dir ´mySamples/´

![2d tri](mySamples/2dtri.png)
![3d tri](mySamples/3dtri.png)
![buff](mySamples/buffertest.png)
![bunny](mySamples/bunny.png)
![clipping2](mySamples/clipping2.png)
![clipping1](mySamples/clipping1.png)
![flag](mySamples/flag.png)